var
    timexe = require("timexe"),
    async = require("async"),
    funcTimeMap = {},
    idFuncMap = {},
    idTimeMap = {},
    genUUID = function(interval,idx){
        return interval.split(" ").join("%20")+"_"+idx;
    },
    exec = function(opts){
      var interval = opts["interval"] || "n/a";
      if(interval!=="n/a"){
          if(funcTimeMap.hasOwnProperty(interval)){
              var funcs = funcTimeMap[interval];
              if( typeof funcs === "object" ){
                funcs.forEach(function(func,idx){
                  if(typeof func === "function"){
                    func();
                    var uuid = genUUID(interval,idx);
                    idFuncMap[uuid].calls+=1;
                  }
                })
              }
          }
      }
    },
    addTick = function(opts){
      var interval = opts["interval"] || "n/a";
      if(interval!=="n/a"){
          var result = timexe(interval, function(){
              exec({interval:interval});
          });
          idTimeMap[interval]=result.id;
      }
    },
    flatten = function(opts){},
    removeInterval = function(opts){
        var interval = opts["interval"] || "";
        if(funcTimeMap.hasOwnProperty(interval)){

            //delete ids in idfuncmap mapped to this interval
            idfuncMapKeys = Object.keys(idFuncMap);
            idfuncMapKeys.forEach(function(idfuncMapKey){
                if(idFuncMap[idfuncMapKey]["interval"] === interval)
                    delete idFuncMap[idfuncMapKey]
            });

            //remove interval from timexe
            timexe.remove(idTimeMap[interval]);

            //remove interval from idTimeMap
            delete idTimeMap[interval];

            //remove interval from funcTimeMap
            delete funcTimeMap[interval];
        }
    },
    _gut = module.exports = {}
;

_gut.add = function(opts){
    var
        intervals = opts["intervals"] || {},
        intervalKeys = Object.keys(intervals),
        results = {status:"successful",ids:{}}
    ;
    intervalKeys.forEach(function(intervalKey){
        results["ids"][intervalKey]=[];
        if(!funcTimeMap.hasOwnProperty(intervalKey)){
            funcTimeMap[intervalKey]=[];
        }
        var uuids = [],
            funcs = intervals[intervalKey],
            offset_sz = funcTimeMap[intervalKey].length
        ;
        funcs.forEach(function(func,idx){
            var
                i_idx = (idx+offset_sz),
                uuid = genUUID(intervalKey,i_idx);
            ;
            uuids[idx]=uuid;
            idFuncMap[uuid]={interval:intervalKey,idx:i_idx,calls:0,func:func};
        })
        funcTimeMap[intervalKey]=funcTimeMap[intervalKey].concat(funcs);
        results["ids"][intervalKey]=uuids;
        addTick({interval:intervalKey});
    });
    return results;
};

_gut.get = function(opts){
    if(!opts || !opts.hasOwnProperty("ids")){
        return idFuncMap;
    }else{
        var result = {}, ids = opts["ids"];
        ids.forEach(function(id){
            result[id]=idFuncMap[id];
        });
        return result;
    }
};

_gut.update = function(opts){

    var
        ids = opts["ids"] || {},
        idKeys = Object.keys(ids),
        intervals = opts["intervals"] || {},
        intervalKeys = Object.keys(intervals),
        result = {status:"successful"}
    ;

    idKeys.forEach(function(idKey){
        if(idFuncMap.hasOwnProperty(idKey))
        {
            var
                interval = idFuncMap[idKey]["interval"],
                idx = idFuncMap[idKey]["idx"],
                func = ids[idKey]
            ;
            funcTimeMap[interval][idx]=func;
        }
    });

    intervalKeys.forEach(function(intervalKey){
        if(funcTimeMap.hasOwnProperty(intervalKey)){
            var funcs = intervals[intervalKey];

            //delete ids in idfuncmap mapped to this interval
            idfuncMapKeys = Object.keys(idFuncMap);
            idfuncMapKeys.forEach(function(idfuncMapKey){
                if(idFuncMap[idfuncMapKey]["interval"] === intervalKey)
                    delete idFuncMap[idfuncMapKey]
            });

            //create new ids like in add
            funcs.forEach(function(func,idx){
                var uuid = genUUID(intervalKey,idx);
                idFuncMap[uuid]={interval:intervalKey,idx:idx,calls:0,func:func};
            });

            //replace func list in interval;
            funcTimeMap[intervalKey] = funcs;
        }
    });

    return result;
};

_gut.remove = function(opts){
    var
        ids = opts["ids"] || [],
        intervals = opts["intervals"] || [],
        result = {status:"successful"}
    ;
    ids.forEach(function(id){
        if(idFuncMap.hasOwnProperty(id))
        {
            var
                interval = idFuncMap[id]["interval"],
                idx = idFuncMap[id]["idx"],
                func = ids[id]
                ;
            delete funcTimeMap[interval][idx];
            delete idFuncMap[id];
            //if interval is empty remove the interval
        }
    });

    intervals.forEach(function(interval){
        removeInterval({interval:interval});
    });

    return result;
};

_gut.destroy = function(){
    var intervals = Object.keys(funcTimeMap);
    intervals.forEach(function(interval){
        removeInterval({interval:interval});
    });
};