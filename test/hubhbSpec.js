var
    hubhb = require("../hubhb"),
    chai = require("chai"),
    spies = require("chai-spies"),
    moment = require('moment'),
    dummy = function(){
    },
    dummy2 = function(){
    },
    dummy_spy,
    dummy2_spy
;

chai.use(spies);
expect = chai.expect;





describe("hubhb",function(){
  describe("add()",function(){

      beforeEach(function(){
          dummy_spy = chai.spy(dummy);
          dummy2_spy = chai.spy(dummy2);
      })

      it("should return result with status and ids",function(){
          var res = hubhb.add({ intervals:{ "* * * * * *":[dummy] } });
          expect(res).to.be.an("object");
          expect(res).to.have.property("status");
          expect(res).to.have.property("ids");
      });

      it("should execute one function atleast once",function(done){
          var res = hubhb.add({ intervals:{ "* * * * * *":[dummy_spy] } });
            setTimeout(function(){
                expect(dummy_spy).to.have.been.called.at.least(1);
                hubhb.destroy();
                done();
            },1000)
      });

      it("should execute more than one function atleast once",function(done){
          var res = hubhb.add({ intervals:{ "* * * * * *":[dummy_spy,dummy2_spy] } });
          setTimeout(function(){
              expect(dummy_spy).to.have.been.called.at.least(1);
              expect(dummy2_spy).to.have.been.called.at.least(1);
              hubhb.destroy();
              done();
          },1000)
      });

      it("should execute once at a specific time",function(done){
          this.timeout(3000);
          //Get current time + 2 sec
          //Set the function to run at that time
          var
              opts = { intervals:{}},
              time = moment().add(2, 's').unix()
          ;
          opts["intervals"]["@"+time]=[dummy_spy];
          var res = hubhb.add(opts);

          //wait 2 seconds
          //check if it executed only once.
          setTimeout(function(){
              expect(dummy_spy).to.have.been.called(1);
              hubhb.destroy();
              done();
          },2000)
      })
  });
});
